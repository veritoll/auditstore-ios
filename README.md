# AuditStore for iOS

## Installation

### Swift Package Manager

The [Swift Package Manager] is a tool for managing distribution of source code written in Swift. To integrate AuditStore with your Swift library or application, add it as a dependency to your `Package.swift`:

```swift
    dependencies: [
        .package(url: "https://bitbucket.org/veritoll/auditstore-ios", .branch:("master")),
    ],
    targets: [
        .target(name: "your-target-name", dependencies: ["AuditStore"]),
    ],
```

### Xcode 12

It is also possible to add the Swift Package within your projects build configuration. Go to the **Project** and then **Swift Packages**. Use the same information for the repository as above.

## Configuration

You will need a few items to properly utilize **AuditStore for iOS**:

 1. An API key
 2. A partner key
 3. A unique identity (at runtime)
 
For an example application please refer to the [AuditStore iOS Example](https://bitbucket.org/veritoll/auditstore-ios-example) app.
