// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "Audit Store for iOS",
    platforms: [
        .iOS(.v11),
    ],
    products: [
        .library(
            name: "AuditStore",
            targets: ["AuditStore"]),
    ],
    dependencies: [],
    targets: [
        .binaryTarget(
            name: "AuditStore",
            path: "Frameworks/AuditStore.xcframework"),
        //.binaryTarget(
        //    name: "AuditStore",
        //    url: "https://bitbucket.org/veritoll/auditstore-ios/downloads/AuditStore.xcframework.zip",
        //    checksum: "3952f4514bc1c9d3a474b3394545d26313866ddf438e8773d56c4a4c68bfc1a6")
    ]
)
